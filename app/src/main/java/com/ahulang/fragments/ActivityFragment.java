package com.ahulang.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ahulang.Activity.ViewActivitiesActivity;
import com.ahulang.Helper.DatabaseHelperClass;
import com.ahulang.Model.ActivitiesModelClass;
import com.ahulang.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ActivityFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ActivityFragment extends Fragment {
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";


  private String mParam1;
  private String mParam2;

  public ActivityFragment() {
  }


  public static ActivityFragment newInstance(String param1, String param2) {
    ActivityFragment fragment = new ActivityFragment();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mParam1 = getArguments().getString(ARG_PARAM1);
      mParam2 = getArguments().getString(ARG_PARAM2);
    }
  }

  @Override
  public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    //inisialisasi
    final EditText edittext_title,edittext_content;
    Button button_add,button_view;
    edittext_title = view.findViewById(R.id.edittext_title);
    edittext_content = view.findViewById(R.id.edittext_content);
    button_add = view.findViewById(R.id.button_add);
    button_view = view.findViewById(R.id.button_view);


//ketika button di tambah
    button_add.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //nampung text ke variable
        String stringName = edittext_title.getText().toString();
        String stringEmail = edittext_content.getText().toString();

        //validasi jika data null
        if (stringName.length() <=0 || stringEmail.length() <=0){
          Toast.makeText(getContext(), "Enter All Data", Toast.LENGTH_SHORT).show();
        }
        //jika tidak null
        else {
          //inisialisasi DatabaseHelperClass
          DatabaseHelperClass databaseHelperClass = new DatabaseHelperClass(getContext());
          //inisialisasi activitiesModelClass
          ActivitiesModelClass activitiesModelClass = new ActivitiesModelClass(stringName,stringEmail);
          //add data activity
          databaseHelperClass.addActivities(activitiesModelClass);
          Toast.makeText(getContext(), "Add Activities Successfully", Toast.LENGTH_SHORT).show();
          getActivity().finish();
          startActivity(getActivity().getIntent());
        }
      }
    });


    //ketika button view di klik
    button_view.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //pindah halaman
        Intent intent = new Intent(getActivity(), ViewActivitiesActivity.class);
        startActivity(intent);
      }
    });
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    //inisialisasi layout
    return inflater.inflate(R.layout.fragment_sms, container, false);
  }
}
