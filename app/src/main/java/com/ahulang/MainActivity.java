package com.ahulang;

import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import com.ahulang.fragments.HomeFragment;
import com.ahulang.fragments.NotificationFragment;
import com.ahulang.fragments.ActivityFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
  //inisialisasi bottom navigation
  BottomNavigationView bottomNavigation;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    //inisialisasi bottom navigation
    bottomNavigation = findViewById(R.id.bottom_navigation);
    bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
    //menempatkan layout untuk setiap fragment
    openFragment(HomeFragment.newInstance("", ""));
  }

  public void openFragment(Fragment fragment) {
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    transaction.replace(R.id.container, fragment);
    transaction.addToBackStack(null);
    transaction.commit();
  }

  //menyesuaikan layout fragment
  //aksi ketika layout di klik
  BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
      new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
          switch (item.getItemId()) {
            case R.id.navigation_home:
              openFragment(HomeFragment.newInstance("", ""));
              return true;
            case R.id.navigation_sms:
              openFragment(ActivityFragment.newInstance("", ""));
              return true;
            case R.id.navigation_notifications:
              openFragment(NotificationFragment.newInstance("", ""));
              return true;
          }
          return false;
        }
      };

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
  }
}
