package com.ahulang.Activity;


import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ahulang.Adapter.ActivitiesAdapterClass;
import com.ahulang.Helper.DatabaseHelperClass;
import com.ahulang.Model.ActivitiesModelClass;
import com.ahulang.R;

import java.util.List;

public class ViewActivitiesActivity extends AppCompatActivity {

    //inisialisasi
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_activities);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        DatabaseHelperClass databaseHelperClass = new DatabaseHelperClass(this);
        List<ActivitiesModelClass> activitiesModelClassList = databaseHelperClass.getActivitieseList();

        //validasi jika list ada datanya
        if (activitiesModelClassList.size() > 0){
            ActivitiesAdapterClass activitiesModelClass = new ActivitiesAdapterClass(activitiesModelClassList
                    ,ViewActivitiesActivity.this);
            recyclerView.setAdapter(activitiesModelClass);
        }else {
            Toast.makeText(this, "There is no activities in the database", Toast.LENGTH_SHORT).show();
        }
    }
}