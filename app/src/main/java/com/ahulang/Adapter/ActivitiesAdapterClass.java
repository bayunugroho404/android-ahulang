package com.ahulang.Adapter;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.ahulang.Helper.DatabaseHelperClass;
import com.ahulang.Model.ActivitiesModelClass;
import com.ahulang.R;

import java.util.List;

public class ActivitiesAdapterClass extends RecyclerView.Adapter<ActivitiesAdapterClass.ViewHolder> {

    //inisialisasi
    List<ActivitiesModelClass> activities;
    Context context;
    DatabaseHelperClass databaseHelperClass;

    //constructor
    public ActivitiesAdapterClass(List<ActivitiesModelClass> activities, Context context) {
        this.activities = activities;
        this.context = context;
        databaseHelperClass = new DatabaseHelperClass(context);
    }


    //inisialisasi view
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.activities_item_list,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final ActivitiesModelClass activitiesModelClass = activities.get(position);

        //set data
        holder.textViewID.setText(Integer.toString(activitiesModelClass.getId()));
        holder.editText_Name.setText(activitiesModelClass.getTitle());
        holder.editText_Email.setText(activitiesModelClass.getContent());

        //ketika button edit di klik
        holder.button_Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringName = holder.editText_Name.getText().toString();
                String stringEmail = holder.editText_Email.getText().toString();

                databaseHelperClass.updateActivities(new ActivitiesModelClass(activitiesModelClass.getId(),stringName,stringEmail));
                notifyDataSetChanged();
                ((Activity) context).finish();
                context.startActivity(((Activity) context).getIntent());
            }
        });

        //ketika button hapus di klik
        holder.button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelperClass.deleteActivities(activitiesModelClass.getId());
                activities.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    //set jumlah semua list
    @Override
    public int getItemCount() {
        return activities.size();
    }

    //inisialisasi id yang ada di layout
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewID;
        EditText editText_Name;
        EditText editText_Email;
        Button button_Edit;
        Button button_delete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewID = itemView.findViewById(R.id.text_id);
            editText_Name = itemView.findViewById(R.id.edittext_title);
            editText_Email = itemView.findViewById(R.id.edittext_content);
            button_delete = itemView.findViewById(R.id.button_delete);
            button_Edit = itemView.findViewById(R.id.button_edit);

        }
    }
}